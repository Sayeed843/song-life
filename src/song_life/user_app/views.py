from django.shortcuts import render
from user_app import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
# Create your views here.


def index(request):
    return render(request,"index.html",{})


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse("user_app:user_index"))


def user_registration(request):
    print("******** User Registration")
    user_form = forms.UserForm()
    user_profile_form = forms.UserProfileInfoForm()

    if request.method == "POST":
        print("**************POST")
        user_form = forms.UserForm(request.POST)
        user_profile_form = forms.UserProfileInfoForm(request.POST)
        print("**********FORM: "+str(user_form))

        if user_form.is_valid() and user_profile_form.is_valid():
            print("******************* IS_VALID")
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = user_profile_form.save(commit=False)
            profile.user = user

            if "user-profile" in request.FILES:
                profile.user_profile_pic = request.FIELS["user-profile"]

            profile.save()
            print("Going to Redirect!!!")
            return HttpResponseRedirect(reverse("user_app:user_index"))

    context_dict = {
        "user_form":user_form,
        "profile_form":user_profile_form,
    }
    return render(request,"user_app/user_register.html",context_dict)




def user_login(request):

    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        user =authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse("user_app:user_index"))

            else:
                return HttpResponse("Account is not active")

        else:
            print("Someone try to get access with wrong information")
            print("Username: {} and Password: {}".format(username, password))
            return HttpResponse("Email or Password is not correct")
    return render(request, "user_app/user_login.html",{})
