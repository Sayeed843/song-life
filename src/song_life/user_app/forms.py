from django import forms
from django.contrib.auth.models import User
from user_app import models


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ("first_name", "last_name", "username", "password")


class UserProfileInfoForm(forms.ModelForm):
    class Meta():
        model = models.UserProfileInfo
        fields = ("user_profile_pic",)
