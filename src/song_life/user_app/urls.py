from django.urls import path
from user_app import views


app_name="user_app"

urlpatterns = [
    path("", views.index, name="user_index"),
    path("register/", views.user_registration, name="user_register"),
    path("login/", views.user_login, name="user_login"),
    path("logout/", views.user_logout, name="user_logout"),
]
