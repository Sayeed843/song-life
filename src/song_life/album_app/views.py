from django.shortcuts import render
from album_app import models
from django.urls import reverse_lazy
from django.views.generic import (View,
                                  TemplateView,
                                  ListView,
                                  DetailView,
                                  CreateView,
                                  UpdateView,
                                  DeleteView,
                                  )
# Create your views here.


class IndexView(TemplateView):
    template_name = "index.html"


class AlbumListView(ListView):
    model = models.Album
    context_object_name = "album_list"
    template_name = "album_app/album_list.html"


class AlbumDetailView(DetailView):
    model = models.Album
    pk = "id"
    context_object_name = "album_details"
    template_name = "album_app/album_details.html"


class AlbumCreateView(CreateView):
    model = models.Album
    fields = ("album_name", "album_author", "album_logo", "album_tags")
    pk = "id"

class AlbumUpdateView(UpdateView):
    model = models.Album
    fields = ("album_name", "album_author", "album_logo", "album_tags")



class AlbumDeleteView(DeleteView):
    model = models.Album
    success_url = reverse_lazy("album_app:album_list")


class SongCreateView(CreateView):
    mdoel = models.Song
    fields = ("song_title", "song_duration", )
