from django.db import models
from multiselectfield import MultiSelectField
from django.urls import reverse
# Create your models here.


#SONG FILEPATH




class Album(models.Model):
    BANGLA = "BAN"
    ROCK = "ROC"
    POP = "POP"
    FOLK = "FOK"
    SOFT = "SOF"
    METAL = "MTAL"
    HARDROCK = "HRK"

    TAGS_CHOICES = [
        (BANGLA, "Bangla"),
        (ROCK, "Rock"),
        (POP, "Pop"),
        (FOLK, "Folk"),
        (SOFT, "Soft"),
        (METAL, "Metal"),
        (HARDROCK, "Hard Rock"),
    ]

    album_name   = models.CharField(max_length=264)
    album_author = models.CharField(max_length=264)
    album_logo   = models.ImageField(upload_to="album-logo")
    album_tags   = MultiSelectField(max_length  = 4*3,
                                    max_choices = 3,
                                    choices     = TAGS_CHOICES,
                                    default     = BANGLA,
                                  )


    def is_upperclass(self):
        return self.album_tags in {self.BANGLA, self.SOFT}


    def __str__(self):
        return self.album_name


    def get_absolute_url(self):
        return reverse("album_app:album_list", kwargs={"pk":self.pk})


# ********************** Song Folder ************************ #
def song_uplopad_path(instance, filename):
    album_name = instance.album.album_name
    return f"album_{album_name}/song/{filename}"

# ********************** Song Folder ************************ #

class Song(models.Model):
    album   = models.ForeignKey(Album,
                            on_delete    = models.CASCADE,
                            related_name ="songs")
    song_title    = models.CharField(max_length=264)
    song_file     = models.FileField(upload_to=song_uplopad_path, blank=False)
    song_duration = models.DecimalField(max_digits=3, decimal_places=2)
    song_favorite = models.BooleanField(default=False)
    song_like     = models.PositiveIntegerField(default=0)


    def __str__(self):
        return self.song_title
