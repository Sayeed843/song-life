# Generated by Django 3.0.7 on 2020-07-01 03:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('album_name', models.CharField(max_length=264)),
                ('album_author', models.CharField(max_length=264)),
                ('album_logo', models.ImageField(upload_to='album-logo')),
                ('album_tags', models.CharField(choices=[('BANGLA', 'Bangla'), ('ROCK', 'Rock'), ('POP', 'Pop'), ('FOLK', 'Folk'), ('SOFT', 'Soft'), ('METAL', 'Metal'), ('HARDROCK', 'Hard Rock')], default='BANGLA', max_length=8)),
            ],
        ),
        migrations.CreateModel(
            name='Song',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('song_title', models.CharField(max_length=264)),
                ('song_duration', models.DecimalField(decimal_places=2, max_digits=2)),
                ('song_favorite', models.BooleanField(default=False)),
                ('song_like', models.PositiveIntegerField()),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='songs', to='album_app.Album')),
            ],
        ),
    ]
