# Generated by Django 3.0.7 on 2020-07-02 05:59

import album_app.models
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('album_app', '0005_auto_20200701_0402'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='song_file',
            field=models.FileField(default=django.utils.timezone.now, upload_to=album_app.models.song_uplopad_path),
            preserve_default=False,
        ),
    ]
