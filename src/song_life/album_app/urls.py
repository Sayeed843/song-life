from django.conf import settings
from django.conf.urls.static import static

from django.urls import path
from album_app import views




app_name = "album_name"

urlpatterns = [
    path("list/", views.AlbumListView.as_view(), name="album_list"),
    path("details/<int:pk>", views.AlbumDetailView.as_view(), name="album_details"),
    path("create/", views.AlbumCreateView.as_view(), name="album_create"),
    path("update/<int:pk>", views.AlbumUpdateView.as_view(), name="album_update"),
    path("delete/<int:pk>", views.AlbumDeleteView.as_view(), name="album_delete"),

    # Song Create, Edit, and Delete
    path("create/<int:pk>/songs", views.SongCreateView.as_view(), name="song_create"),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
